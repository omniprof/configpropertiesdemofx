package com.kenfogel.propertiestest;

import com.kenfogel.configpropertiesdemofx.propertybean.PropertyBean;
import com.kenfogel.configpropertiesdemofx.propertybean.propertiesmanager.PropertiesManager;
import java.io.IOException;
import java.util.logging.Level;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author omni_
 */
public class PropertiesTest {
    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(PropertiesTest.class);

    @Test
    public void testProperties() throws IOException  {
        PropertyBean properties = new PropertyBean();
        PropertiesManager pm = new PropertiesManager();
        pm.loadTextProperties(properties, "", "MailConfig");
        LOG.debug(properties.toString());
        assertEquals("Bullwinkle Moose", properties.getUserName());
    }

}
